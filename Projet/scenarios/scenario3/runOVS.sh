#!/bin/bash

# Create connection to OVSDB
sudo ovs-vsctl set-manager "ptcp:6640"

# Create SWITCH
sudo ovs-vsctl add-br lot1 -- set Bridge lot1 fail-mode=secure

# Connection to controller
sudo ovs-vsctl set-controller lot1 tcp:127.0.0.1:6633

# Clear flows table
sudo ovs-ofctl del-flows lot1

# Configure OFP for fist switch
sudo ovs-ofctl add-flow --protocols=OpenFlow10,OpenFlow11,OpenFlow12,OpenFlow13,OpenFlow14 lot1 actions=normal,controller
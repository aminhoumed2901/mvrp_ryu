if sudo ovs-vsctl show | grep -e "ovs1" -q; then
    sudo ovs-vsctl del-br ovs1
fi

if sudo ovs-vsctl show | grep -e "ovs2" -q; then
    sudo ovs-vsctl del-br ovs2
fi
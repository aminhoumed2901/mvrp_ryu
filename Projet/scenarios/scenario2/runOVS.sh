#!/bin/bash
# Create connection to OVSDB
sudo ovs-vsctl set-manager "ptcp:6640"

# Create SWITCH
sudo ovs-vsctl add-br ovs1 -- set Bridge ovs1 fail-mode=secure
sudo ovs-vsctl add-br ovs2 -- set Bridge ovs2 fail-mode=secure

# Create p0 port for first switch
sudo ovs-vsctl add-port ovs1 p0 trunks=11,12 -- set Interface p0 type=patch -- set interface p0 options:peer=p1
# sudo ovs-vsctl set port p0 vlan_mode=native-tagged

# Create p1 port for second switch
sudo ovs-vsctl add-port ovs2 p1 trunks=11,12 -- set Interface p1 type=patch -- set interface p1 options:peer=p0
# sudo ovs-vsctl set port p1 vlan_mode=native-tagged

# Connection to controller
sudo ovs-vsctl set-controller ovs1 tcp:127.0.0.1:6633
sudo ovs-vsctl set-controller ovs2 tcp:127.0.0.1:6633

# Clear flows table
sudo ovs-ofctl del-flows ovs1
sudo ovs-ofctl del-flows ovs2

# Configure OFP for fist switch
sudo ovs-vsctl set bridge ovs1 protocols=OpenFlow10,OpenFlow11,OpenFlow12,OpenFlow13,OpenFlow14
sudo ovs-ofctl add-flow --protocols=OpenFlow10,OpenFlow11,OpenFlow12,OpenFlow13,OpenFlow14 ovs1 actions=normal,controller

# Configure OFP for second switch
sudo ovs-vsctl set bridge ovs2 protocols=OpenFlow10,OpenFlow11,OpenFlow12,OpenFlow13,OpenFlow14
sudo ovs-ofctl add-flow --protocols=OpenFlow10,OpenFlow11,OpenFlow12,OpenFlow13,OpenFlow14 ovs2 actions=normal,controller
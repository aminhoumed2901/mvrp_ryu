#!/bin/bash

netdev=$1
switch="ovs1"
ip addr flush dev $netdev
ip link set $netdev down
ovs-vsctl del-port ${switch} $netdev

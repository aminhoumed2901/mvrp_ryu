#!/bin/bash

netdev=$1
ip addr flush dev $netdev
ip link set $netdev down
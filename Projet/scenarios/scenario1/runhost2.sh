#!/bin/bash

VM_FOLDER="../VM/"
IFACE="eth1_1"
HOST="host2"
SWITCH="OVS1"
MAC_ADDR=$(printf '52:54:00:%02x:%02x:%02x' $((RANDOM%256)) $((RANDOM%256)) $((RANDOM%256)))
FILE_UP="/etc/ovs-ifup.sh"
FILE_DOWN="/etc/ovs-ifdown.sh"

sudo kvm -m 512 -smp 1 \
	-net nic,macaddr=${MAC_ADDR},model=virtio \
	-net tap,id=${IFACE},ifname=${IFACE},script=${FILE_UP},downscript=${FILE_DOWN} \
	-hda ${VM_FOLDER}${HOST}.qcow2 \
	-boot order=dc,menu=on \
	-name ${HOST}

#!/bin/bash

VM_FOLDER="../VM/"
HOST="host1"
MAC_ADDR=$(printf '52:54:00:%02x:%02x:%02x' $((RANDOM%256)) $((RANDOM%256)) $((RANDOM%256)))
FILE_UP="./intSwitch/ovs-ifup.sh"
FILE_DOWN="./intSwitch/ovs-ifdown.sh"

sudo kvm -m 512 -smp 1 \
    -device e1000,mac=${MAC_ADDR},netdev="e1" \
    -netdev socket,id="e1" \
	-hda ${VM_FOLDER}${HOST}.qcow2 \
	-boot order=dc,menu=on \
	-name ${HOST}

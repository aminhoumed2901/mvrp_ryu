# -*- coding: utf-8 -*-

from ryu.base import app_manager

from ryu.controller import ofp_event 

from ryu.controller.handler import MAIN_DISPATCHER, CONFIG_DISPATCHER
from ryu.controller.handler import set_ev_cls

from ryu.ofproto import ofproto_v1_0

from ryu.lib.packet import packet, vlan, ethernet, ether_types

class demo(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_0.OFP_VERSION]
    
    def __init__(self, *args, **kwargs):
        super(demo, self).__init__(*args, **kwargs)

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):        
        msg         = ev.msg
        datapath    = msg.datapath
        ofproto     = datapath.ofproto
        parser      = datapath.ofproto_parser
        port_no     = msg.in_port
        pkt         = packet.Packet(msg.data)
        eth         = pkt.get_protocols(ethernet.ethernet)[0]
        vlan_header = pkt.get_protocols(vlan.vlan)
        vlanID      = 0

        vlanID = vlan_header[0].vid if len(vlan_header) > 0 else 0 
        if(vlanID !=0):
            self.logger.info("\033[1;32m[%s]\033[0m packet %s, VLAN %s, from port %s", 
                    str(type(pkt.protocols[-1])).split('.')[-1].replace('\'>',''),
                    eth, 
                    vlanID,
                    port_no
            )
        
        data = None
        if msg.buffer_id == ofproto.OFP_NO_BUFFER:
            data = msg.data
        
        out = parser.OFPPacketOut(
            datapath=datapath, buffer_id=msg.buffer_id, in_port=port_no,
            actions=[], data=data)

        datapath.send_msg(out)